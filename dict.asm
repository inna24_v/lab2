global find_word
extern string_equals

section .text
find_word:
.loop:
        test rsi,rsi ; проверяем на постуто список слов в словаре
        jz .end
        push rsi  ; последнее слово в словаре
        push rdi  ; искомое слово
        add rsi, 8
        call string_equals
        pop rdi
        pop rsi
        test rax, rax   ; проверка того, что вернула функция
        jnz .end 		; если не 0, тогда слова совпадают
        mov rsi, [rsi]  ; берем следующее слово из словаря
        jmp .loop
.end:
        mov rax, rsi
        ret
