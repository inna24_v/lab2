%include "colon.inc"
%include "words.inc"
extern read_word
extern find_word
extern print_newline
extern print_string
extern print_err
extern string_length
extern exit
extern print_uint

global _start

section .rodata
errormsg: db "try another key", 10, 0

section .text
_start:
        sub rsp, 255 ; создаем буфер
        mov rdi, rsp  ; передаем вершину буфера в read_word 
        mov rsi, 255 ; передаём размер буфера в read_word
        call read_word
        mov rdi, rax 
        mov rsi, next ; передаем адрес последнего слова
        call find_word
        test rax, rax
        jz .loss
        add rax, 8
        push rax  
        mov rdi, rax
        call string_length
        pop rdi 		; восстанавливаем ссылку на слово в rdi (сохраненное из rax)
        add rdi, rax
        inc rdi			; и место для нуль-терминатора
        call print_string
        call print_newline

        call exit

        .loss:
        mov rdi, errormsg
        call print_err
		call exit




