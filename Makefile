all: main # вызывается по умолчанию

main: main.o dict.o lib.o  # зависимости
	ld -o main main.o dict.o lib.o

main.o: main.asm colon.inc words.inc # проверяется на наличие
	nasm -felf64 main.asm

lib.o: lib.asm
	nasm -felf64 lib.asm

dict.o: dict.asm
	nasm -felf64 dict.asm

clean: # удаляет объектные файлы командой ( make Makefile clean )
	rm -f *.o main