%define next 0
%macro colon 2
%%next: dq next        	;pointer
db %1,0               	;key
%2:                     ;label
%define next %%next
%endmacro
