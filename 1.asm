global _start
%define BUFFER_SIZE 32
section .text
_start:
	xor rax, rax
	mov rax, rbp
	sub rax, rsp
	mov rbx, 8
	push rdx
	xor rdx, rdx
	div rbx
	pop rdx
	mov rdi, rax
	call print_uint
	call print_newline
	
	xor rax, rax
	xor rcx, rcx
	.loop:
		pop rax
		inc rcx
		push rcx
		mov rdi, rcx
		call print_uint
		call print_newline
		pop rcx
		jmp .loop

	
print_uint:
	push rbp
	mov rax, rdi     ;кладем число в аккумулятор
	mov rdi, 10       ;кладём в rdi делитель
	mov rbp, rsp      ;сохраняем адрес вершины стека
	sub rsp, BUFFER_SIZE
	dec rbp          ;выделяем место под нуль в вершине стека
	mov byte[rbp], 0 ;записываем 0 на вершину стека
	.loop:
		xor rdx, rdx
		div rdi
		or rdx, 0x30   ;остаток + код первой цифры в ASCII
		dec rbp
		mov [rbp], dl  ;сохраняем 1 цифру в стек
		cmp rax, 0 ;
		jne .loop
	mov rdi, rbp   ;помещаем аргумент для print_string
	call print_string
	add rsp, BUFFER_SIZE
	pop rbp	
	ret
	
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0   ;check if current symbol is null-terminator
		je .end
		inc rax
		jmp .loop
	.end:
		ret

print_string:
	call string_length
	mov rdx, rax    ;the amount of bites to write  
	mov rax, 1      ;'write' syscall idetifier
	mov rsi, rdi    ;where do we take data from
	mov rdi, 1      ;std file descriptor
	syscall
	ret
	
print_char:
	push rdi       ;save char in the stack
	mov rax, 1
	mov rdx, 1
	mov rsi, rsp   ;the adress of the topmost element in the stack
	pop rdi
	mov rdi, 1
	syscall
	ret

print_newline:
	mov rdi, 10
	call print_char
	ret